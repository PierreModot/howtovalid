const SRC_ROOT = 'src'
const OBJ_ROOT = 'obj'
const BIN_ROOT = 'bin'
const DOC_ROOT = 'doc'
const DATA_ROOT = 'data'
const TMP_ROOT = 'tmp'
const FLAGS = `\
CC = g++\n\
CCFLAGS = -Wall -std=c++20 -ggdb\n\
INCLUDES = -I/usr/include/SDL2\n\
\n\
LD = g++\n\
LDFLAGS =\n\
LIBS =\n\
\n\
LIBS_TXT =\n\
LIBS_SDL = -lSDL2 -lSDL2_image -lSDL2_ttf\n\
LIBS_TEST =\n\
`
const TARGETS = [
	{ name: 'HowToValid', exclude: [/^txt\//, /^test\//], include: [], extra: ['$(LIBS_SDL)'] },
	{ name: 'HowToValid_console', exclude: [/^sdl\//, /^test\//], include: [], extra: ['$(LIBS_TXT)'] },
	{ name: 'HowToValid_tests', exclude: [/^sdl\//, /^txt\//], include: [], extra: ['$(LIBS_TEST)'] },
]

const { readdirSync, readFileSync, writeFileSync } = require('node:fs')
const { join: joinPath, dirname } = require('node:path')

/** @param {string} path @return {string[]} */
function getFiles(path) {
	const children = readdirSync(path, { withFileTypes: true })
	return children.flatMap(child => {
		if (child.isDirectory()) return getFiles(joinPath(path, child.name))
		else if (child.isFile()) return [joinPath(path, child.name)]
		else return []
	})
}

/** @param {string} file @return {string[]} */
function getImmediateDeps(file) {
	return readFileSync(file, { encoding: 'utf-8' })
		.split('\n')
		.map(line => line.match(/^#include\s+"(.*?)"/)?.[1])
		.filter(include => include)
		.map(path => joinPath(dirname(file), path))
}

/** @param {string[]} files @return {{name: string, parents: string[]}[]} */
function getDeps(files) {
	/** @type {{[key: string]: string[]}} */
	const deps = {}
	files.forEach(file => { deps[file] = getImmediateDeps(file) })
	let changed = true;
	while (changed) {
		changed = false
		for (let file in deps) {
			deps[file].forEach(child => {
				if (deps[child]) deps[child].forEach(newchild => {
					if (!deps[file].includes(newchild)) {
						deps[file].push(newchild)
						changed = true
					}
				})
			})
		}
	}
	const ret = []
	for (let file in deps) {
		ret.push({ name: file, parents: deps[file].filter(child => files.includes(child)) })
	}
	return ret
}

const tree = getDeps(getFiles(SRC_ROOT))
const cpp = tree.filter(file => file.name.endsWith('.cpp'))
const folders = cpp
	.map(file => `${OBJ_ROOT}${dirname(file.name).slice(SRC_ROOT.length)}`)
	.filter((path, idx, arr) => !arr.slice(idx + 1).some(other => path == other))

let result = ''
result += `${FLAGS}\n`
result += `all: make_dirs ${TARGETS.map(target => `${BIN_ROOT}/${target.name}`).join(' ')}\n`
result += `nothing:\n`
result += `docs:\n\tdoxygen ${DOC_ROOT}/Doxyfile\n`
result += `format:\n\tclang-format -i src/**/*\n`
result += `clean:\n\trm -rf ${OBJ_ROOT}/* ${BIN_ROOT}/* ${DOC_ROOT}/html ${TMP_ROOT}/*\n`
result += `make_dirs:\n\tmkdir -p ${SRC_ROOT} ${OBJ_ROOT} ${BIN_ROOT} ${DATA_ROOT} ${DOC_ROOT} ${TMP_ROOT} ${folders.join(' ')}\n`
TARGETS.forEach(target => {
	const objFiles = cpp
		.filter(file => !target.exclude.some(exclude => exclude.test(`${file.name.slice(SRC_ROOT.length + 1, -4)}`)) ||
			target.include.some(include => include.test(`${file.name.slice(SRC_ROOT.length + 1, -4)}`)))
		.map(file => `${OBJ_ROOT}${file.name.slice(SRC_ROOT.length, -3)}o`)
		.join(' ')
	result += `${BIN_ROOT}/${target.name}: ${objFiles}\n\t$(LD) ${objFiles} -o ${BIN_ROOT}/${target.name} $(LDFLAGS) $(LIBS)${target.extra.map(extra => ` ${extra}`)}\n`
})
cpp.forEach(file => {
	const objName = `${OBJ_ROOT}${file.name.slice(SRC_ROOT.length, -3)}o`
	result += `${objName}: ${file.name} ${file.parents.join(' ')}\n\t$(CC) -c $(CCFLAGS) $(INCLUDES) ${file.name} -o ${objName}\n`
})

writeFileSync('Makefile', result, { encoding: 'utf-8' })
