#include "Projectile.h"
#include <math.h>

Projectile::Projectile(double x, double y, double angle, double vitesse,
                       unsigned int degats)
	: Entite() {
	this->positionX = x;
	this->positionY = y;
	this->vitesse = vitesse;
	this->vitesseParDefaut = vitesse;
	this->degats = degats;
	this->texture = 80;
	this->angle = angle;
	this->collision = 1;
}

void Projectile::avancer() {
	this->positionX += cos(this->angle) * this->vitesse;
	this->positionY += sin(this->angle) * this->vitesse;
}

double Projectile::getAngle() const { return this->angle; }
