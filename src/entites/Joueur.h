#ifndef _JOUEUR_H
#define _JOUEUR_H

#include "Entite.h"

/**
 * @brief Un joueur.
 */
class Joueur : public Entite {
public:
	/**
	 * @brief Construit un joueur.
	 * @param x Sa position X.
	 * @param y Sa position Y.
	 */
	Joueur(double x, double y);
};

#endif
