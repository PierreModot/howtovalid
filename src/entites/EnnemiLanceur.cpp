#include "EnnemiLanceur.h"

EnnemiLanceur::EnnemiLanceur(std::istream& data) : Ennemi(data) {
	this->texture = 36;
	this->portee = 7;
	this->type = 1;
	this->tempsDepuisDegats = 2020;
	this->offSetTexture = 36;
}
