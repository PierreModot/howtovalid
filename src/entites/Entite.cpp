#include "Entite.h"
#include "../objets/Objet.h"
#include <math.h>

// Getters

int Entite::getTempsDepuisDegats() const { return this->tempsDepuisDegats; }

int Entite::getCollision() const { return this->collision; }

unsigned int Entite::getPV() const { return this->pv; }

unsigned int Entite::getPVMax() const { return this->pvMax; }

double Entite::getPositionX() const { return this->positionX; }

double Entite::getPositionY() const { return this->positionY; }

double Entite::getVitesse() const { return this->vitesse; }

double Entite::getVitesseDefaut() const { return this->vitesseParDefaut; }

unsigned int Entite::getDegats() const { return this->degats; }

const std::vector<Objet*>& Entite::getConstInventaire() const {
	return this->inventaire;
}

const std::unordered_set<char>& Entite::getConstEviteTuiles() const {
	return this->eviteTuiles;
}

// Setters

void Entite::setPV(unsigned int x) {
	if (x > this->pvMax)
		this->pv = this->pvMax;
	else
		this->pv = x;
}

void Entite::setResistance(int x) { this->resistance = x; }

void Entite::resetVitesse() { this->vitesse = this->vitesseParDefaut; }

void Entite::changerVitesse(double vitesse) { this->vitesse = vitesse; }

// Actions globales

void Entite::tic() { this->tempsDepuisDegats++; }

void Entite::subirDegats(unsigned int degatsSubis) {
	unsigned int degatsActu = ceil(degatsSubis / (double)this->resistance);
	if (degatsActu >= this->pv)
		this->pv = 0;
	else
		this->pv -= degatsActu;
	this->tempsDepuisDegats = 0;
}

void Entite::soigner(unsigned int soins) {
	if (this->pv + soins >= this->pvMax)
		this->pv = this->pvMax;
	else
		this->pv += soins;
}

void Entite::attaquer(Entite* victime) const {
	victime->subirDegats(this->degats);
}

void Entite::utiliserObjet() {
	if (this->inventaire.size() > 0) {
		if (this->inventaire.back()->utiliser(*this)) {
			this->inventaire.pop_back();
		}
	}
}

// Déplacement

void Entite::placer(double x, double y) {
	this->positionX = x;
	this->positionY = y;
}

void Entite::deplacerDroite(double max) {
	int positionActuX = this->positionX;
	this->positionX += fmin(this->vitesse, max);

	// Animation
	if (this->varAnim == 0 && positionActuX != this->positionX) {
		this->texture = 2 + this->offSetTexture;
		this->varAnim = 1;
	} else {
		this->texture = 0 + this->offSetTexture;
		this->varAnim = 0;
	}
}

void Entite::deplacerGauche(double max) {
	int positionActuX = this->positionX;
	this->positionX -= fmin(this->vitesse, max);

	// Animation
	if (this->varAnim == 0 && positionActuX != this->positionX) {
		this->texture = 8 + this->offSetTexture;
		this->varAnim = 1;
	} else {
		this->texture = 6 + this->offSetTexture;
		this->varAnim = 0;
	}
}

void Entite::deplacerBas(double max) {
	int positionActuY = this->positionY;
	this->positionY += fmin(this->vitesse, max);

	// Animation
	if (this->varAnim == 0 && positionActuY != this->positionY) {
		this->texture = 5 + this->offSetTexture;
		this->varAnim = 1;
	} else {
		this->texture = 3 + this->offSetTexture;
		this->varAnim = 0;
	}
}

void Entite::deplacerHaut(double max) {
	int positionActuY = this->positionY;
	this->positionY -= fmin(this->vitesse, max);

	// Animation
	if (this->varAnim == 0 && positionActuY != this->positionY) {
		this->texture = 11 + this->offSetTexture;
		this->varAnim = 1;
	} else {
		this->texture = 9 + this->offSetTexture;
		this->varAnim = 0;
	}
}

// Animation

void Entite::animAttaqueGauche() { this->texture = 14 + this->offSetTexture; }

void Entite::animAttaqueDroite() { this->texture = 12 + this->offSetTexture; }

void Entite::animAttaqueHaut() { this->texture = 15 + this->offSetTexture; }

void Entite::animAttaqueBas() { this->texture = 13 + this->offSetTexture; }

// Construction et destruction

Entite::Entite() {
	this->positionX = 0;
	this->positionY = 0;
	this->pv = 1;
	this->pvMax = 1;
	this->vitesse = 1;
	this->vitesseParDefaut = 1;
	this->degats = 1;
	this->gagne = false;
	this->texture = 0;
	this->varAnim = 0;
	this->offSetTexture = 0;
	this->tempsDepuisDegats = 2020;
	this->collision = 0;
	this->decompteAttaque = 0;
	this->resistance = 1;
	this->tailleMaxInventaire = 0;
	this->inventaire.clear();
	this->eviteTuiles.clear();
}
