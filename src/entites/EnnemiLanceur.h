#ifndef _ENNEMI_LANCEUR_H
#define _ENNEMI_LANCEUR_H

#include "Ennemi.h"
#include <istream>

/**
 * @brief Un ennemi lanceur.
 */
class EnnemiLanceur : public Ennemi {
public:
	/**
	 * @brief Construit un ennemi lanceur selon des données.
	 */
	EnnemiLanceur(std::istream& data);
};

#endif
