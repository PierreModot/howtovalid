#ifndef _ENTITE_H
#define _ENTITE_H

#include <unordered_set>
#include <vector>

class Objet;

/**
 * @brief Une entité.
 */
class Entite {
public:
	// État de l'entité

	/**
	 * @brief L'indice de la texture utilisée par l'entité.
	 */
	int texture;

	/**
	 * @brief Décalage global de la texture pour les animations.
	 */
	int offSetTexture;

	/**
	 * @brief L'inventaire de l'entité.
	 */
	std::vector<Objet*> inventaire;

	/**
	 * @brief Taille de l'inventaire de l'entité.
	 */
	unsigned int tailleMaxInventaire;

	/**
	 * @brief Temps avant que l'entité puisse attaquer.
	 */
	int decompteAttaque;

	/**
	 * @brief Indique si l'entité pense avoir gagné.
	 */
	bool gagne;

	// Getters

	/**
	 * @brief Retourne le temps écoulé depuis les derniers dégâts subits par l'entité.
	 * @return Le temps écoulé depuis les derniers dégâts subits par l'entité.
	 */
	int getTempsDepuisDegats() const;

	/**
	 * @brief Retourne le niveau de collision maximal sur lequel l'entité peut marcher.
	 * @return Le niveau de collision maximal sur lequel l'entité peut marcher.
	 */
	int getCollision() const;

	/**
	 * @brief Retourne les pv de l'entité.
	 * @return Les pv de l'entité.
	 */
	unsigned int getPV() const;

	/**
	 * @brief Retourne les pv max de l'entité.
	 * @return Les pv max de l'entité.
	 */
	unsigned int getPVMax() const;

	/**
	 * @brief Retourne la position X de l'entité.
	 * @return La position X de l'entité.
	 */
	double getPositionX() const;

	/**
	 * @brief Retourne la position Y de l'entité.
	 * @return La position Y de l'entité.
	 */
	double getPositionY() const;

	/**
	 * @brief Retourne la vitesse de l'entité.
	 * @return La vitesse de l'entité.
	 */
	double getVitesse() const;

	/**
	 * @brief Retourne la vitesse par defaut de l'entité.
	 * @return La vitesse par défaut de l'entité.
	 */
	double getVitesseDefaut() const;

	/**
	 * @brief Retourne les dégats infligés par l'entité lors d'une attaque.
	 * @return Retourne les dégats infligés par l'entité lors d'une attaque.
	 */
	unsigned int getDegats() const;

	/**
	 * @brief Retourne une référence constante vers l'inventaire de l'entité.
	 * @return Une référence constante vers inventaire de l'entité.
	 */
	const std::vector<Objet*>& getConstInventaire() const;

	/**
	 * @brief Retourne une référence constante vers la liste des tuiles sur lesquelles l'entité de marchera pas volontairement.
	 * @return Une référence constante vers la liste des tuiles sur lesquelles l'entité de marchera pas volontairement.
	 */
	const std::unordered_set<char>& getConstEviteTuiles() const;

	// Setters

	/**
	 * @brief Change les pv de l'entité.
	 * @param pv Les nouveaux pv.
	 */
	void setPV(unsigned int pv);

	/**
	 * @brief Change la résistance de l'entité.
	 * @param resistance La nouvelle résistance.
	 */
	void setResistance(int resistance);

	/**
	 * @brief Remet la vitesse de l'entité à sa valeur par defaut.
	 */
	void resetVitesse();

	/**
	 * @brief Change la vitesse de l'entité.
	 * @param vitesse La nouvelle vitesse de l'entité.
	 */
	void changerVitesse(double vitesse);

	// Actions globales

	/**
	 * @brief Actions effectuées à chaque tour de boucle.
	 */
	void tic();

	/**
	 * @brief Diminue les points de vie de l'entité.
	 * @param degats Nombre de points de vie que perd l'entité.
	 */
	void subirDegats(unsigned int degats);

	/**
	 * @brief Augmente les points de vie de l'entité.
	 * @param soins Nombre de points de vie que gagne l'entité.
	 */
	void soigner(unsigned int soins);

	/**
	 * @brief Attaque un autre entité.
	 * @param victime Pointeur sur l'entité attaquée.
	 */
	void attaquer(Entite* victime) const;

	/**
	 * @brief Utilise le dernier objet récupéré.
	 */
	void utiliserObjet();

	// Déplacement

	/**
	 * @brief Place l'entité aux coordonées indiquées.
	 * @param x Position x où l'entité sera placée.
	 * @param y Position y où l'entité sera placée.
	 */
	void placer(double x, double y);

	/**
	 * @brief Déplace l'entité à droite.
	 * @param max La distance maximale.
	 */
	void deplacerDroite(double max);

	/**
	 * @brief Déplace l'entite à gauche.
	 * @param max La distance maximale.
	 */
	void deplacerGauche(double max);

	/**
	 * @brief Déplace l'entite en bas.
	 * @param max La distance maximale.
	 */
	void deplacerBas(double max);

	/**
	 * @brief Déplace l'entite en haut.
	 * @param max La distance maximale.
	 */
	void deplacerHaut(double max);

	// Animation

	/**
	 * @brief Lance l'animation d'attaque à gauche.
	 */
	void animAttaqueGauche();

	/**
	 * @brief Lance l'animation d'attaque à droite.
	 */
	void animAttaqueDroite();

	/**
	 * @brief Lance l'animation d'attaque en haut.
	 */
	void animAttaqueHaut();

	/**
	 * @brief Lance l'animation d'attaque en bas.
	 */
	void animAttaqueBas();

protected:
	/**
	 * @brief Les pv de l'entité.
	 */
	unsigned int pv;

	/**
	 * @brief Les pv max de l'entité.
	 */
	unsigned int pvMax;

	/**
	 * @brief La position X de l'entité.
	 */
	double positionX;

	/**
	 * @brief La position Y de l'entité.
	 */
	double positionY;

	/**
	 * @brief La vitesse de l'entité.
	 */
	double vitesse;

	/**
	 * @brief La vitesse par défaut de l'entité.
	 */
	double vitesseParDefaut;

	/**
	 * @brief Les dégâts infligés par l'entité lors d'une attaque.
	 */
	unsigned int degats;

	/**
	 * @brief L'état de l'animation de l'entité.
	 */
	int varAnim;

	/**
	 * @brief Le temps écoulé depuis les derniers dégâts subits par l'entité.
	 */
	int tempsDepuisDegats;

	/**
	 * @brief Le niveau de collision maximal sur lequel l'entité peut marcher.
	 */
	int collision;

	/**
	 * @brief La liste des tuiles sur lesquelles l'entité de marchera pas volontairement.
	 */
	std::unordered_set<char> eviteTuiles;

	/**
	 * @brief La résistance de l'entité.
	 */
	int resistance;

	// Construction et destruction

	/**
	 * @brief Construit une entité.
	 */
	Entite();
};

#endif
