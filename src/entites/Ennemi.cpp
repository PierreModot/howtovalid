#include "Ennemi.h"

Ennemi::Ennemi(std::istream& data) : Entite() {
	data >> this->positionX;
	data >> this->positionY;
	data >> this->pv;
	this->pvMax = this->pv;
	this->degats = 2;
	this->portee = 4;
	this->type = 0;
}

int Ennemi::getPortee() const { return this->portee; }

int Ennemi::getType() const { return this->type; }
