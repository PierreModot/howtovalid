#ifndef _CONFIG_H
#define _CONFIG_H

#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

/**
 * @brief Une configuration.
 */
class Config {
public:
	/**
	 * @brief Lit un fichier de configuration.
	 * @param path Le chemin du fichier.
	 */
	Config(std::string path);

	/**
	 * @brief Lit un fichier de configuration.
	 * @param path Le chemin du fichier.
	 */
	Config(const char* path);

	/**
	 * @brief Récupère un chemin.
	 * @param key La clé du chemin.
	 * @return Le chemin correspondant.
	 */
	const std::string& getPath(const std::string& key) const;

	/**
	 * @brief Récupère un chemin.
	 * @param key La clé du chemin.
	 * @return Le chemin correspondant.
	 */
	const std::string& getPath(const char* key) const;

	/**
	 * @brief Récupère un texte.
	 * @param key La clé du texte.
	 * @return Le texte correspondant.
	 */
	const std::vector<std::string>& getText(const std::string& key) const;

	/**
	 * @brief Récupère un texte.
	 * @param key La clé du texte.
	 * @return Le texte correspondant.
	 */
	const std::vector<std::string>& getText(const char* key) const;

	/**
	 * @brief Récupère une chaîne.
	 * @param key La clé de la chaîne.
	 * @return La chaîne correspondante.
	 */
	const std::string& getString(const std::string& key) const;

	/**
	 * @brief Récupère une chaîne.
	 * @param key La clé de la chaîne.
	 * @return La chaîne correspondante.
	 */
	const std::string& getString(const char* key) const;

	/**
	 * @brief Récupère un entier.
	 * @param key La clé de l'entier.
	 * @return L'entier correspondant.
	 */
	int getInt(const std::string& key) const;

	/**
	 * @brief Récupère un entier.
	 * @param key La clé de l'entier.
	 * @return L'entier correspondant.
	 */
	int getInt(const char* key) const;

	/**
	 * @brief Récupère un flottant.
	 * @param key La clé du flottant.
	 * @return Le flottant correspondant.
	 */
	double getDouble(const std::string& key) const;

	/**
	 * @brief Récupère un flottant.
	 * @param key La clé du flottant.
	 * @return Le flottant correspondant.
	 */
	double getDouble(const char* key) const;

	/**
	 * @brief Affiche la configuration (debug).
	 */
	void logContents() const;

private:
	/**
	 * @brief Les chemins stockés.
	 */
	std::unordered_map<std::string, std::string> paths;

	/**
	 * @brief Les textes stockés.
	 */
	std::unordered_map<std::string, std::vector<std::string>> texts;

	/**
	 * @brief Les chaînes stockées.
	 */
	std::unordered_map<std::string, std::string> strings;

	/**
	 * @brief Les entiers stockés.
	 */
	std::unordered_map<std::string, int> ints;

	/**
	 * @brief Les flottants stockés.
	 */
	std::unordered_map<std::string, double> doubles;

	/**
	 * @brief Lit un caractère comme chiffre hexadécimal.
	 * @param hex Le caractère hexadécimal.
	 * @return Le nombre lu 0 - 15.
	 */
	static int parseHex1(char hex);

	/**
	 * @brief Lit deux chiffres hexadécimaux comme entier signé.
	 * @param left Le premier caractère hexadécimal.
	 * @param right Le deuxième caractère hexadécimal.
	 * @return La nombre lu -128 - 127.
	 */
	static char parseHex2(char left, char right);

	/**
	 * @brief Lit un chemin.
	 * @param input Le `stream` d'où le chemin est lu.
	 * @param output Le `string` dans lequel le chemin est écrit.
	 */
	static void parsePath(std::ifstream& input, std::string& output);

	/**
	 * @brief Lit un texte.
	 * @param input Le `stream` d'où le texte est lu.
	 * @param output Le `vector<string>` dans lequel le chemin est écrit.
	 */
	static void parseText(std::ifstream& input,
	                      std::vector<std::string>& output);

	/**
	 * @brief Lit une chaîne.
	 * @param input le `stream` d'où la chaîne est lue.
	 * @param output Le `string` dans lequel la chaîne est écrite.
	 */
	static void parseString(std::ifstream& input, std::string& output);
};

#endif
