#define _USE_MATH_DEFINES

#include "Partie.h"
#include "../entites/EnnemiLanceur.h"
#include "../entites/EnnemiMalin.h"
#include <cmath>
#include <fstream>

// Construction et destruction

Partie::Partie(Config* config) {
	// Chargement de la configuration
	this->config = config;
	this->cartes.clear();
	int ncartes = config->getInt("niveau.nombre");
	for (int i = 0; i < ncartes; i++) {
		std::string nomCarte = std::string("niveau.") + std::to_string(i);
		this->cartes.push_back(this->config->getPath(nomCarte));
	}
	this->cartes.shrink_to_fit();

	// Remise à zéro

	this->carte = nullptr;
	this->joueur = nullptr;
	this->difficulte = 1;
	this->ennemis.clear();
	this->projectiles.clear();

	this->resetPartie(this->cartes[0]);

	this->partieEnCours = false;
	this->niveau = 1;
}

Partie::~Partie() { this->viderPartie(); }

// Getters

const Carte& Partie::getConstCarte() const { return *this->carte; }

const Joueur& Partie::getConstJoueur() const { return *this->joueur; }

const std::vector<Ennemi*>& Partie::getConstEnnemis() const {
	return this->ennemis;
}

const std::vector<Projectile*>& Partie::getConstProjectiles() const {
	return this->projectiles;
}

const std::vector<Dialogue>& Partie::getConstDialogues() const {
	return this->dialogues;
}

const std::vector<Particule*>& Partie::getConstParticules() const {
	return this->particules;
}

bool Partie::getPartieEnCours() const { return this->partieEnCours; }

int Partie::getDifficulte() const { return this->difficulte; }

bool Partie::fini() {
	return this->joueur->getPV() == 0 || this->joueur->gagne;
}

unsigned int Partie::getNiveau() const { return this->niveau; }

bool Partie::gagne() const { return this->niveau == this->cartes.size(); }

// Actions de la boucle de jeu

void Partie::actionsAutomatiques() {
	// Avancement de l'état des particules
	for (int i = this->particules.size() - 1; i >= 0; i--) {
		if (this->particules[i]->getTempsRestant() == 0) {
			delete this->particules[i];
			this->particules.erase(this->particules.begin() + i);
		} else {
			this->particules[i]->tic();
		}
	}

	// Avancement de l'état des entités
	this->joueur->tic();
	for (Ennemi* ennemi : this->ennemis)
		ennemi->tic();
	for (Projectile* projectile : this->projectiles)
		projectile->tic();

	// Animation du joueur et des ennemis
	this->animationAuto(*this->joueur);
	for (Ennemi* ennemi : this->ennemis)
		this->animationAuto(*ennemi);

	// Décrémentation du décompte d'attaque du joueur
	if (this->joueur->decompteAttaque != 0)
		this->joueur->decompteAttaque--;

	// Activation des tuiles
	std::vector<ChangementTuile> changementsTuiles;

	this->joueur->resetVitesse();
	this->carte
		->getTuile(this->joueur->getPositionX() + 0.5,
	               ceil(this->joueur->getPositionY()))
		->subirTuile(*this->joueur, this->joueur->getPositionX() + 0.5,
	                 ceil(this->joueur->getPositionY()), changementsTuiles);

	for (Ennemi* ennemi : this->ennemis) {
		ennemi->resetVitesse();
		this->carte
			->getTuile(ennemi->getPositionX() + 0.5,
		               ceil(ennemi->getPositionY()))
			->subirTuile(*ennemi, ennemi->getPositionX() + 0.5,
		                 ceil(ennemi->getPositionY()), changementsTuiles);
	}

	// Mort des ennemis
	for (int i = this->ennemis.size() - 1; i >= 0; i--) {
		if (this->ennemis[i]->getPV() <= 0) {
			delete this->ennemis[i];
			this->ennemis.erase(this->ennemis.begin() + i);
		}
	}

	// Application des changements de tuiles
	for (const auto& changement : changementsTuiles) {
		this->carte->setType(changement.x, changement.y, changement.type);
		this->carte->setTexture(changement.x, changement.y, changement.texture);
	}

	// Actions des ennemis et des projectiles
	actionsEnnemis();
	actionsProjectiles();
}

void Partie::actionClavier(char touche) {
	std::vector<unsigned int> ennemisAttaque;
	ennemisAttaque.clear();

	if (touche == 'p' && this->fini() && this->partieEnCours) {
		// Si on est sur l'écran de victoire, l'écran de transition ou l'écran de défaite
		if (!this->gagne() && this->joueur->gagne) {
			// Si on est sur l'écran de transition, on passe au niveau suivant
			this->resetPartie(this->cartes[this->niveau]);
			this->niveau++;
			this->appliquerDiff();
		} else {
			// Si on est sur l'écran de victoire ou l'écran de défaite, en revient au menu
			this->niveau = 1;
			this->resetPartie(this->cartes[0]);
			this->partieEnCours = false;
			return;
		}
	}

	if (!this->partieEnCours) {
		// Si on est sur le menu principal
		switch (touche) {
		case 'p':
			// Lancement de la partie
			this->partieEnCours = true;
			this->appliquerDiff();
			break;
		case 'q':
			// Cycle à gauche de la difficulté
			if (!this->partieEnCours) {
				this->difficulte--;
				if (this->difficulte < 0)
					this->difficulte = 2;
			}
			break;
		case 'd':
			// Cycle à droite de la difficulté
			if (!this->partieEnCours) {
				this->difficulte++;
				if (this->difficulte > 2)
					this->difficulte = 0;
			}
			break;
		}
	} else {
		std::vector<unsigned int> ennemisDeplacement;
		ennemisDeplacement.clear();
		double maxDeplacement;
		switch (touche) {
		case 'a':
			// Marche lente
			this->joueur->changerVitesse(this->joueur->getVitesse() * 0.5);
			break;
		case 'q':
			// Déplacement à gauche
			maxDeplacement = this->maxDeplacementGauche(*this->joueur);
			this->getIndicesEnnemisDansBoite(
				this->joueur->getPositionX() - maxDeplacement - 1,
				this->joueur->getPositionX() - 1,
				this->joueur->getPositionY() - Partie::MAX_PREC,
				this->joueur->getPositionY() + Partie::MAX_PREC,
				ennemisDeplacement);
			for (const auto& idx : ennemisDeplacement) {
				Ennemi* ennemi = this->ennemis[idx];
				double dx =
					this->joueur->getPositionX() - ennemi->getPositionX() - 1;
				if (dx < maxDeplacement)
					maxDeplacement = dx;
			}
			this->joueur->deplacerGauche(maxDeplacement);
			break;
		case 'd':
			// Déplacement à droite
			maxDeplacement = this->maxDeplacementDroite(*this->joueur);
			this->getIndicesEnnemisDansBoite(
				this->joueur->getPositionX() + 1,
				this->joueur->getPositionX() + maxDeplacement + 1,
				this->joueur->getPositionY() - Partie::MAX_PREC,
				this->joueur->getPositionY() + Partie::MAX_PREC,
				ennemisDeplacement);
			for (const auto& idx : ennemisDeplacement) {
				Ennemi* ennemi = this->ennemis[idx];
				double dx =
					ennemi->getPositionX() - this->joueur->getPositionX() - 1;
				if (dx < maxDeplacement)
					maxDeplacement = dx;
			}
			this->joueur->deplacerDroite(maxDeplacement);
			break;
		case 's':
			// Déplacement en bas
			maxDeplacement = this->maxDeplacementBas(*this->joueur);
			this->getIndicesEnnemisDansBoite(
				this->joueur->getPositionX() - Partie::MAX_PREC,
				this->joueur->getPositionX() + Partie::MAX_PREC,
				this->joueur->getPositionY() + 1,
				this->joueur->getPositionY() + maxDeplacement + 1,
				ennemisDeplacement);
			for (const auto& idx : ennemisDeplacement) {
				Ennemi* ennemi = this->ennemis[idx];
				double dy =
					ennemi->getPositionY() - this->joueur->getPositionY() - 1;
				if (dy < maxDeplacement)
					maxDeplacement = dy;
			}
			this->joueur->deplacerBas(maxDeplacement);
			break;
		case 'z':
			// Déplacement en haut
			maxDeplacement = this->maxDeplacementHaut(*this->joueur);
			this->getIndicesEnnemisDansBoite(
				this->joueur->getPositionX() - Partie::MAX_PREC,
				this->joueur->getPositionX() + Partie::MAX_PREC,
				this->joueur->getPositionY() - maxDeplacement - 1,
				this->joueur->getPositionY() - 1, ennemisDeplacement);
			for (const auto& idx : ennemisDeplacement) {
				Ennemi* ennemi = this->ennemis[idx];
				double dy =
					this->joueur->getPositionY() - ennemi->getPositionY() - 1;
				if (dy < maxDeplacement)
					maxDeplacement = dy;
			}
			this->joueur->deplacerHaut(maxDeplacement);
			break;
		case 'o':
			// Attaque en haut
			if (this->joueur->decompteAttaque == 0) {
				this->getIndicesEnnemisDansBoite(
					this->joueur->getPositionX() - 0.5,
					this->joueur->getPositionX() + 0.5,
					this->joueur->getPositionY() - 1.5,
					this->joueur->getPositionY() - 0.5, ennemisAttaque);
				this->joueur->animAttaqueHaut();
				this->particules.push_back(
					new Particule(this->joueur->getPositionX(),
				                  this->joueur->getPositionY() - 0.5, M_PI / 2,
				                  82, 0, -0.25, 0, 1, 1));
				this->joueur->decompteAttaque = 10;
			}
			break;
		case 'k':
			// Attaque à gauche
			if (this->joueur->decompteAttaque == 0) {
				this->getIndicesEnnemisDansBoite(
					this->joueur->getPositionX() - 1.5,
					this->joueur->getPositionX() - 0.5,
					this->joueur->getPositionY() - 0.5,
					this->joueur->getPositionY() + 0.5, ennemisAttaque);
				this->joueur->animAttaqueGauche();
				this->particules.push_back(new Particule(
					this->joueur->getPositionX() - 0.5,
					this->joueur->getPositionY(), 0, 82, -0.25, 0, 0, 1, 1));
				this->joueur->decompteAttaque = 10;
			}
			break;
		case 'l':
			// Attaque en bas
			if (this->joueur->decompteAttaque == 0) {
				this->getIndicesEnnemisDansBoite(
					this->joueur->getPositionX() - 0.5,
					this->joueur->getPositionX() + 0.5,
					this->joueur->getPositionY() + 0.5,
					this->joueur->getPositionY() + 1.5, ennemisAttaque);
				this->joueur->animAttaqueBas();
				this->particules.push_back(
					new Particule(this->joueur->getPositionX(),
				                  this->joueur->getPositionY() + 0.5,
				                  M_PI * 3 / 2, 82, 0, 0.25, 0, 1, 1));
				this->joueur->decompteAttaque = 10;
			}
			break;
		case 'm':
			// Attaque à droite
			if (this->joueur->decompteAttaque == 0) {
				this->getIndicesEnnemisDansBoite(
					this->joueur->getPositionX() + 0.5,
					this->joueur->getPositionX() + 1.5,
					this->joueur->getPositionY() - 0.5,
					this->joueur->getPositionY() + 0.5, ennemisAttaque);
				this->joueur->animAttaqueDroite();
				this->particules.push_back(new Particule(
					this->joueur->getPositionX() + 0.5,
					this->joueur->getPositionY(), M_PI, 82, 0.25, 0, 0, 1, 1));
				this->joueur->decompteAttaque = 10;
			}
			break;
		case ' ':
			// Utilisation d'un objet
			this->joueur->utiliserObjet();
			break;
		}

		// Attaque des ennemis
		for (const auto& i : ennemisAttaque) {
			this->joueur->attaquer(this->ennemis[i]);
		}

		// Mort des ennemis
		for (int i = this->ennemis.size() - 1; i >= 0; i--) {
			if (this->ennemis[i]->getPV() <= 0) {
				delete this->ennemis[i];
				this->ennemis.erase(this->ennemis.begin() + i);
			}
		}
	}
}

// Remise à zéro

void Partie::viderPartie() {
	if (this->carte != nullptr)
		delete this->carte;
	if (this->joueur != nullptr)
		delete this->joueur;
	for (const auto& ennemi : this->ennemis)
		delete ennemi;
	for (const auto& projectile : this->projectiles)
		delete projectile;
	for (const auto& particule : this->particules)
		delete particule;
	this->carte = nullptr;
	this->joueur = nullptr;
	this->ennemis.clear();
	this->projectiles.clear();
	this->dialogues.clear();
	this->particules.clear();
}

void Partie::resetPartie(const std::string& cheminCarte) {
	this->viderPartie();
	std::ifstream fCarte(cheminCarte);

	// Initialisation de la carte
	this->carte = new Carte(fCarte);

	// Initialisation du joueur
	double jx, jy;
	fCarte >> jx;
	fCarte >> jy;
	this->joueur = new Joueur(jx, jy);
	this->joueur->decompteAttaque = 0;

	// Initialisations des ennemis
	int nbrEnnemis;
	int typeEnnemi;
	fCarte >> nbrEnnemis;
	for (int i = 0; i < nbrEnnemis; i++) {
		fCarte >> typeEnnemi;
		switch (typeEnnemi) {
		case 0:
			this->ennemis.push_back(new Ennemi(fCarte));
			break;
		case 1:
			this->ennemis.push_back(new EnnemiMalin(fCarte));
			break;
		case 2:
			this->ennemis.push_back(new EnnemiLanceur(fCarte));
			break;
		}
	}

	// Initialisation des dialogues
	int nbrDialogues;
	fCarte >> nbrDialogues;
	for (int i = 0; i < nbrDialogues; i++) {
		Dialogue dia;
		fCarte >> dia.x >> dia.y;
		dia.texte.clear();
		char chr = ' ';
		std::string str;
		str.clear();
		while (chr == ' ')
			fCarte.get(chr);
		while (!fCarte.eof() && chr != '\n' && chr != '\r') {
			if (chr == '\\') {
				str.shrink_to_fit();
				dia.texte.push_back(str);
				str = std::string();
				str.clear();
			} else
				str.push_back(chr);
			fCarte.get(chr);
		}
		str.shrink_to_fit();
		dia.texte.push_back(str);
		this->dialogues.push_back(dia);
		this->dialogues.shrink_to_fit();
	}
}

void Partie::appliquerDiff() {
	if (this->difficulte == 0)
		this->joueur->setResistance(2);
	if (this->difficulte == 2)
		this->joueur->setPV(10);
	this->joueur->offSetTexture = this->difficulte * 18;
	this->joueur->texture = this->joueur->texture + (this->difficulte * 18);
}

// Actions du jeu

void Partie::actionsEnnemis() {
	for (Ennemi* ennemi : this->ennemis) {
		// Décrémentation du décompte d'attaque de l'ennemi
		if (ennemi->decompteAttaque > 0)
			ennemi->decompteAttaque--;

		switch (ennemi->getType()) {
		case 0: {
			// IA Chasseuse
			int portee = ennemi->getPortee();
			int dx = this->joueur->getPositionX() - ennemi->getPositionX();
			int dy = this->joueur->getPositionY() - ennemi->getPositionY();
			std::vector<unsigned int> indicesEnnemis;
			int direction = 0;
			if (ennemi->decompteAttaque <= 0) {
				// Attaque à droite
				if (this->joueurEstDansBoite(ennemi->getPositionX() + 0.5,
				                             ennemi->getPositionX() + 1.5,
				                             ennemi->getPositionY() - 0.5,
				                             ennemi->getPositionY() + 0.5))
					direction = 1;
				// Attaque à gauche
				else if (this->joueurEstDansBoite(ennemi->getPositionX() - 1.5,
				                                  ennemi->getPositionX() - 0.5,
				                                  ennemi->getPositionY() - 0.5,
				                                  ennemi->getPositionY() + 0.5))
					direction = 2;
				// Attaque en bas
				else if (this->joueurEstDansBoite(ennemi->getPositionX() - 0.5,
				                                  ennemi->getPositionX() + 0.5,
				                                  ennemi->getPositionY() + 0.5,
				                                  ennemi->getPositionY() + 1.5))
					direction = 3;
				// Attaque en haut
				else if (this->joueurEstDansBoite(ennemi->getPositionX() - 0.5,
				                                  ennemi->getPositionX() + 0.5,
				                                  ennemi->getPositionY() - 1.5,
				                                  ennemi->getPositionY() - 0.5))
					direction = 4;
			}
			switch (direction) {
			case 1: {
				// Attaque à droite
				ennemi->animAttaqueDroite();
				ennemi->decompteAttaque = 5;
				this->particules.push_back(new Particule(
					ennemi->getPositionX() + 0.5, ennemi->getPositionY(), M_PI,
					82, 0.25, 0, 0, 1, 1));
				ennemi->attaquer(this->joueur);
			} break;
			case 2: {
				// Attaque à gauche
				ennemi->animAttaqueGauche();
				ennemi->decompteAttaque = 5;
				this->particules.push_back(new Particule(
					ennemi->getPositionX() - 0.5, ennemi->getPositionY(), 0, 82,
					-0.25, 0, 0, 1, 1));
				ennemi->attaquer(this->joueur);
			} break;
			case 3: {
				// Attaque en bas
				ennemi->animAttaqueBas();
				ennemi->decompteAttaque = 5;
				this->particules.push_back(new Particule(
					ennemi->getPositionX(), ennemi->getPositionY() + 0.5,
					M_PI * 3 / 2, 82, 0, 0.25, 0, 1, 1));
				ennemi->attaquer(this->joueur);
			} break;
			case 4: {
				// Attaque en haut
				ennemi->animAttaqueHaut();
				ennemi->decompteAttaque = 5;
				this->particules.push_back(new Particule(
					ennemi->getPositionX(), ennemi->getPositionY() - 0.5,
					M_PI / 2, 82, 0, -0.25, 0, 1, 1));
				ennemi->attaquer(this->joueur);
			} break;
			default: {
				// N'attaque pas
				// Bouge à droite
				if (dx > 0 && dx < portee && dy > -portee && dy < portee)
					direction |= 1;
				// Bouge à gauche
				if (dx > -portee && dx < 0 && dy > -portee && dy < portee)
					direction |= 2;
				// Bouge en bas
				if (dx > -portee && dx < portee && dy > 0 && dy < portee)
					direction |= 4;
				// Bouge en haut
				if (dx > -portee && dx < portee && dy > -portee && dy < 0)
					direction |= 8;
				// Bouge aléatoirement
				if (direction == 0) {
					direction = std::rand() % 100;
					if (direction > 15)
						direction = 0;
				}
				if (direction & 1) {
					// Bouge à droite
					indicesEnnemis.clear();
					double maxDeplacement = this->maxDeplacementDroite(*ennemi);
					this->getIndicesEnnemisDansBoite(
						ennemi->getPositionX() + 1,
						ennemi->getPositionX() + maxDeplacement + 1,
						ennemi->getPositionY() - Partie::MAX_PREC,
						ennemi->getPositionY() + Partie::MAX_PREC,
						indicesEnnemis);
					if (this->joueurEstDansBoite(
							ennemi->getPositionX() + 1,
							ennemi->getPositionX() + maxDeplacement + 1,
							ennemi->getPositionY() - Partie::MAX_PREC,
							ennemi->getPositionY() + Partie::MAX_PREC)) {
						double dx = this->joueur->getPositionX() -
									ennemi->getPositionX() - 1;
						if (dx < maxDeplacement)
							maxDeplacement = dx;
					}
					for (const auto& idx : indicesEnnemis) {
						Ennemi* autre = this->ennemis[idx];
						double dx =
							autre->getPositionX() - ennemi->getPositionX() - 1;
						if (dx < maxDeplacement)
							maxDeplacement = dx;
					}
					ennemi->deplacerDroite(maxDeplacement);
				}
				if (direction & 2) {
					// Bouge à gauche
					indicesEnnemis.clear();
					double maxDeplacement = this->maxDeplacementGauche(*ennemi);
					this->getIndicesEnnemisDansBoite(
						ennemi->getPositionX() - maxDeplacement - 1,
						ennemi->getPositionX() - 1,
						ennemi->getPositionY() - Partie::MAX_PREC,
						ennemi->getPositionY() + Partie::MAX_PREC,
						indicesEnnemis);
					if (this->joueurEstDansBoite(
							ennemi->getPositionX() - maxDeplacement - 1,
							ennemi->getPositionX() - 1,
							ennemi->getPositionY() - Partie::MAX_PREC,
							ennemi->getPositionY() + Partie::MAX_PREC)) {
						double dx = ennemi->getPositionX() -
									this->joueur->getPositionX() - 1;
						if (dx < maxDeplacement)
							maxDeplacement = dx;
					}
					for (const auto& idx : indicesEnnemis) {
						Ennemi* autre = this->ennemis[idx];
						double dx =
							ennemi->getPositionX() - autre->getPositionX() - 1;
						if (dx < maxDeplacement)
							maxDeplacement = dx;
					}
					ennemi->deplacerGauche(maxDeplacement);
				}
				if (direction & 4) {
					// Bouge en bas
					indicesEnnemis.clear();
					double maxDeplacement = this->maxDeplacementBas(*ennemi);
					this->getIndicesEnnemisDansBoite(
						ennemi->getPositionX() - Partie::MAX_PREC,
						ennemi->getPositionX() + Partie::MAX_PREC,
						ennemi->getPositionY() + 1,
						ennemi->getPositionY() + maxDeplacement + 1,
						indicesEnnemis);
					if (this->joueurEstDansBoite(
							ennemi->getPositionX() - Partie::MAX_PREC,
							ennemi->getPositionX() + Partie::MAX_PREC,
							ennemi->getPositionY() + 1,
							ennemi->getPositionY() + maxDeplacement + 1)) {
						double dy = this->joueur->getPositionY() -
									ennemi->getPositionY() - 1;
						if (dy < maxDeplacement)
							maxDeplacement = dy;
					}
					for (const auto& idx : indicesEnnemis) {
						Ennemi* autre = this->ennemis[idx];
						double dy =
							autre->getPositionY() - ennemi->getPositionY() - 1;
						if (dy < maxDeplacement)
							maxDeplacement = dy;
					}
					ennemi->deplacerBas(maxDeplacement);
				}
				if (direction & 8) {
					// Bouge en haut
					indicesEnnemis.clear();
					double maxDeplacement = this->maxDeplacementHaut(*ennemi);
					this->getIndicesEnnemisDansBoite(
						ennemi->getPositionX() - Partie::MAX_PREC,
						ennemi->getPositionX() + Partie::MAX_PREC,
						ennemi->getPositionY() - maxDeplacement - 1,
						ennemi->getPositionY() - 1, indicesEnnemis);
					if (this->joueurEstDansBoite(
							ennemi->getPositionX() - Partie::MAX_PREC,
							ennemi->getPositionX() + Partie::MAX_PREC,
							ennemi->getPositionY() - maxDeplacement - 1,
							ennemi->getPositionY() - 1)) {
						double dy = ennemi->getPositionY() -
									this->joueur->getPositionY() - 1;
						if (dy < maxDeplacement)
							maxDeplacement = dy;
					}
					for (const auto& idx : indicesEnnemis) {
						Ennemi* autre = this->ennemis[idx];
						double dy =
							ennemi->getPositionY() - autre->getPositionY() - 1;
						if (dy < maxDeplacement)
							maxDeplacement = dy;
					}
					ennemi->deplacerHaut(maxDeplacement);
				}
			} break;
			}
		} break;
		case 1: {
			// IA Lanceuse
			// Ne fait qu'attaquer
			if (ennemi->decompteAttaque > 0)
				break;

			// Calcul de angle d'attaque
			double ddx = this->joueur->getPositionX() - ennemi->getPositionX(),
				   ddy = this->joueur->getPositionY() - ennemi->getPositionY();
			double dmx = fmax(std::abs(ddx), std::abs(ddy));
			if (dmx > ennemi->getPortee())
				break;
			double dpx = ddx / dmx, dpy = ddy / dmx;
			double ang = atan2(ddy, ddx);

			// Animation
			if (ang < -M_PI * 3.0 / 4.0)
				ennemi->animAttaqueGauche();
			else if (ang < -M_PI / 4.0)
				ennemi->animAttaqueHaut();
			else if (ang < M_PI / 4.0)
				ennemi->animAttaqueDroite();
			else if (ang < M_PI * 3.0 / 4.0)
				ennemi->animAttaqueBas();
			else
				ennemi->animAttaqueGauche();

			// Création de l'attaque
			this->particules.push_back(new Particule(
				ennemi->getPositionX() + cos(ang) * 0.5,
				ennemi->getPositionY() + sin(ang) * 0.5, ang + M_PI, 82,
				cos(ang) * 0.25, sin(ang) * 0.25, 0, 1, 1));
			projectiles.push_back(new Projectile(
				ennemi->getPositionX() + dpx, ennemi->getPositionY() + dpy, ang,
				0.25, ennemi->getDegats()));
			ennemi->decompteAttaque = 10;
		} break;
		}
	}
}

void Partie::actionsProjectiles() {
	for (Projectile* projectile : this->projectiles) {
		// Avance du projectile
		bool broken = false;
		projectile->avancer();
		unsigned int posX = projectile->getPositionX() + 0.5,
					 posY = ceil(projectile->getPositionY());

		// Collision avec le joueur
		if (this->joueurEstDansBoite(projectile->getPositionX() - 0.5,
		                             projectile->getPositionX() + 0.5,
		                             projectile->getPositionY() - 0.5,
		                             projectile->getPositionY() + 0.5)) {
			projectile->attaquer(this->joueur);
			projectile->subirDegats(projectile->getPVMax());
			broken = true;
		}
		if (broken)
			break;

		// Collision avec les ennemis
		std::vector<unsigned int> idxEnnemis;
		this->getIndicesEnnemisDansBoite(
			projectile->getPositionX() - 0.5, projectile->getPositionX() + 0.5,
			projectile->getPositionY() - 0.5, projectile->getPositionY() + 0.5,
			idxEnnemis);
		for (const auto& idx : idxEnnemis) {
			// Seul un seul ennemi peut est touché donc on peut le supprimer et invalider l'itérateur
			projectile->attaquer(this->ennemis[idx]);
			projectile->subirDegats(projectile->getPVMax());
			broken = true;
			if (this->ennemis[idx]->getPV() <= 0) {
				delete this->ennemis[idx];
				this->ennemis.erase(this->ennemis.begin() + idx);
			}
			break;
		}
		if (broken)
			break;

		// Collision avec les tuiles
		if (posX < 0 || posY < 0 || posX >= this->carte->getLargeur() ||
		    posY >= this->carte->getHauteur() ||
		    this->carte->getTuile(posX, posY)->collision >
		        projectile->getCollision()) {
			projectile->subirDegats(projectile->getPVMax());
			broken = true;
		}
		if (broken)
			break;
	}

	// Mort des projectiles
	for (int i = this->projectiles.size() - 1; i >= 0; i--) {
		if (this->projectiles[i]->getPV() <= 0) {
			this->particules.push_back(
				new Particule(this->projectiles[i]->getPositionX(),
			                  this->projectiles[i]->getPositionY(), 0, 81, 0));
			delete this->projectiles[i];
			this->projectiles.erase(this->projectiles.begin() + i);
		}
	}
}

void Partie::animationAuto(Entite& entite) {
	int textureActu = entite.texture;
	int offSetText = entite.offSetTexture;
	if (textureActu - offSetText < 12) {
		// Si la texture actuelle est une texture de déplacement, on passe sur l'autre texture de déplacement
		if (textureActu % 3 == 0)
			entite.texture = textureActu + 1;
		else
			entite.texture = textureActu - 1;
	} else {
		// Si la texture actuelle est une texture d'attaque, on passe sur la première texture de déplacement
		entite.texture = (textureActu - offSetText) % 12 * 3 + offSetText;
	}
}

// Calcul de déplacement maximal

double Partie::maxDeplacementGauche(const Entite& entite) const {
	double dep = entite.getPositionX() - floor(entite.getPositionX());

	// En fonction du niveau de collision
	while (dep < entite.getVitesse()) {
		if (this->carte
		            ->getTuile(entite.getPositionX() - dep - 1,
		                       entite.getPositionY())
		            ->collision > entite.getCollision() ||
		    this->carte
		            ->getTuile(entite.getPositionX() - dep - 1,
		                       ceil(entite.getPositionY()))
		            ->collision > entite.getCollision())
			break;
		dep++;
	}

	// En fonction des tuiles exclues
	for (double i = Partie::MIN_STEP; i <= dep; i += Partie::MIN_STEP) {
		char type = this->carte->getType(entite.getPositionX() - i + 0.5,
		                                 ceil(entite.getPositionY()));
		if (entite.getConstEviteTuiles().contains(type)) {
			dep = i - Partie::MIN_STEP;
			break;
		}
	}
	return fmin(dep, entite.getVitesse());
}

double Partie::maxDeplacementDroite(const Entite& entite) const {
	double dep = ceil(entite.getPositionX()) - entite.getPositionX();

	// En fonction du niveau de collision
	while (dep < entite.getVitesse()) {
		if (this->carte
		            ->getTuile(entite.getPositionX() + dep + 1,
		                       entite.getPositionY())
		            ->collision > entite.getCollision() ||
		    this->carte
		            ->getTuile(entite.getPositionX() + dep + 1,
		                       ceil(entite.getPositionY()))
		            ->collision > entite.getCollision())
			break;
		dep++;
	}

	// En fonction des tuiles exclues
	for (double i = Partie::MIN_STEP; i <= dep; i += Partie::MIN_STEP) {
		char type = this->carte->getType(entite.getPositionX() + i + 0.5,
		                                 ceil(entite.getPositionY()));
		if (entite.getConstEviteTuiles().contains(type)) {
			dep = i - Partie::MIN_STEP;
			break;
		}
	}
	return fmin(dep, entite.getVitesse());
}

double Partie::maxDeplacementHaut(const Entite& entite) const {
	double dep = entite.getPositionY() - floor(entite.getPositionY());

	// En fonction du niveau de collision
	while (dep < entite.getVitesse()) {
		if (this->carte
		            ->getTuile(entite.getPositionX(),
		                       entite.getPositionY() - dep - 1)
		            ->collision > entite.getCollision() ||
		    this->carte
		            ->getTuile(ceil(entite.getPositionX()),
		                       entite.getPositionY() - dep - 1)
		            ->collision > entite.getCollision())
			break;
		dep++;
	}

	// En fonction des tuiles exclues
	for (double i = Partie::MIN_STEP; i <= dep; i += Partie::MIN_STEP) {
		char type = this->carte->getType(entite.getPositionX() + 0.5,
		                                 ceil(entite.getPositionY() - i));
		if (entite.getConstEviteTuiles().contains(type)) {
			dep = i - Partie::MIN_STEP;
			break;
		}
	}
	return fmin(dep, entite.getVitesse());
}

double Partie::maxDeplacementBas(const Entite& entite) const {
	double dep = ceil(entite.getPositionY()) - entite.getPositionY();

	// En fonction du niveau de collision
	while (dep < entite.getVitesse()) {
		if (this->carte
		            ->getTuile(entite.getPositionX(),
		                       entite.getPositionY() + dep + 1)
		            ->collision > entite.getCollision() ||
		    this->carte
		            ->getTuile(ceil(entite.getPositionX()),
		                       entite.getPositionY() + dep + 1)
		            ->collision > entite.getCollision())
			break;
		dep++;
	}

	// En fonction des tuiles exclues
	for (double i = Partie::MIN_STEP; i <= dep; i += Partie::MIN_STEP) {
		char type = this->carte->getType(entite.getPositionX() + 0.5,
		                                 ceil(entite.getPositionY() + i));
		if (entite.getConstEviteTuiles().contains(type)) {
			dep = i - Partie::MIN_STEP;
			break;
		}
	}
	return fmin(dep, entite.getVitesse());
}

// Calculs d'entités dans des boîtes

void Partie::getIndicesEnnemisDansBoite(
	double minX, double maxX, double minY, double maxY,
	std::vector<unsigned int>& indices) const {
	for (unsigned int i = 0; i < this->ennemis.size(); i++) {
		Ennemi* ennemi = this->ennemis[i];
		double ex = ennemi->getPositionX(), ey = ennemi->getPositionY();
		if (ex >= minX && ex <= maxX && ey >= minY && ey <= maxY)
			indices.push_back(i);
	}
}

bool Partie::joueurEstDansBoite(double minX, double maxX, double minY,
                                double maxY) const {
	double jx = this->joueur->getPositionX(), jy = this->joueur->getPositionY();
	return jx >= minX && jx <= maxX && jy >= minY && jy <= maxY;
}
