#include "Particule.h"

Particule::Particule(double x, double y, double angle, int texture,
                     unsigned int tempsRestant) {
	this->x = x;
	this->y = y;
	this->vx = 0;
	this->vy = 0;
	this->angle = angle;
	this->vangle = 0;
	this->texture = texture;
	this->vtexture = 0;
	this->tempsRestant = tempsRestant;
}

Particule::Particule(double x, double y, double angle, int texture, double vx,
                     double vy, double vangle, int vtexture,
                     unsigned int tempsRestant) {
	this->x = x;
	this->y = y;
	this->vx = vx;
	this->vy = vy;
	this->angle = angle;
	this->vangle = vangle;
	this->texture = texture;
	this->vtexture = vtexture;
	this->tempsRestant = tempsRestant;
}

void Particule::tic() {
	if (this->tempsRestant == 0)
		return;
	this->x += this->vx;
	this->y += this->vy;
	this->angle += this->vangle;
	this->texture += this->vtexture;
	this->tempsRestant--;
}

double Particule::getX() const { return this->x; }
double Particule::getY() const { return this->y; }
double Particule::getAngle() const { return this->angle; }
int Particule::getTexture() const { return this->texture; }
unsigned int Particule::getTempsRestant() const { return this->tempsRestant; }
