#include "txtJeu.h"
#include "../objets/Objet.h"
#include <math.h>
#include <unordered_set>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#include <unistd.h>
#endif

TxtJeu::TxtJeu(Partie* partie, const Config* config, int maxW, int maxH,
               int zoom, const char* texTuiles, const char* texJoueur,
               const char* texEnnemis)
	: win(fmax(maxW * zoom, 80), fmax(maxH * zoom + 5, 20)) {
	this->partie = partie;
	this->config = config;
	this->maxW = maxW;
	this->maxH = maxH;
	this->zoom = zoom;
	this->texTuiles = texTuiles;
	this->texJoueur = texJoueur;
	this->texEnnemis = texEnnemis;
}

TxtJeu::~TxtJeu() {}

void TxtJeu::boucle(unsigned int mspt) {
	// Remise à zéro de la boucle
	bool ok = true;
	std::unordered_set<char> touches;
#ifndef _WIN32
	timeval st, et;
	gettimeofday(&st, nullptr);
#endif

	// Boucle de jeu
	while (ok) {
		this->win.clear(' ');
		if (!this->partie->getPartieEnCours()) {
			// Écran titre
			this->afficheEcranTitre();
		} else if (this->partie->fini()) {
			if (this->partie->gagne()) {
				// Écran de victoire
				this->afficheEcranVictoire();
			} else if (this->partie->getConstJoueur().gagne) {
				// Écran de transition
				this->afficheEcranSuivant();
			} else {
				// Écran de défaite
				this->afficheEcranDefaite();
			}
		} else {
			// Écran principal
			this->afficheCarte();
			this->afficheInterface();

			// Actions automatiques du jeu
			this->partie->actionsAutomatiques();
		}
		this->win.draw();

		// Attente
#ifdef _WIN32
		Sleep(mspt);
#else
		gettimeofday(&et, nullptr);
		int elapsed =
			(et.tv_sec - st.tv_sec) * 1000000ll + (et.tv_usec - st.tv_usec);
		int waittime = int(mspt) * 1000 - elapsed;
		usleep(fmax(1, waittime));
		gettimeofday(&st, nullptr);
#endif

		// Gestion des touches
		touches.clear();
		char c;
		while ((c = this->win.getCh()) != '\0')
			touches.insert(c);
		if (touches.contains('!')) {
			ok = false;
			continue;
		}
		if (touches.contains('p'))
			this->partie->actionClavier('p');
		if (touches.contains('a'))
			this->partie->actionClavier('a');
		if (touches.contains('z'))
			this->partie->actionClavier('z');
		if (touches.contains('q'))
			this->partie->actionClavier('q');
		if (touches.contains('s'))
			this->partie->actionClavier('s');
		if (touches.contains('d'))
			this->partie->actionClavier('d');
		if (touches.contains('o'))
			this->partie->actionClavier('o');
		if (touches.contains('k'))
			this->partie->actionClavier('k');
		if (touches.contains('l'))
			this->partie->actionClavier('l');
		if (touches.contains('m'))
			this->partie->actionClavier('m');
		if (touches.contains(' '))
			this->partie->actionClavier(' ');
	}
}

// Affichage des différents écrans

void TxtJeu::afficheEcranTitre() {
	this->afficheTexte(0, 0,
	                   this->config->getText("texte.choixPersonnage.texte"));
	this->afficheTexte(0, 4,
	                   this->config->getText("texte.touchesDifficulte.texte"));
	this->afficheTexte(
		0, 8,
		this->config->getText(std::string("texte.personnage.nom.") +
	                          std::to_string(this->partie->getDifficulte())));
	this->afficheTexte(
		0, 12,
		this->config->getText(std::string("texte.personnage.description.") +
	                          std::to_string(this->partie->getDifficulte())));
	this->afficheTexte(0, 16, this->config->getText("texte.commencer.texte"));
}

void TxtJeu::afficheEcranVictoire() {
	this->win.print(0, 0, "Bravo!");
	this->win.print(0, 1, "Vous avez obtenu votre diplôme.");
	this->afficheTexte(0, 3, this->config->getText("texte.continuer.texte"));
}

void TxtJeu::afficheEcranSuivant() {
	this->win.print(0, 0, "Vous y êtes presque!");
	this->afficheTexte(0, 3, this->config->getText("texte.continuer.texte"));
}

void TxtJeu::afficheEcranDefaite() {
	this->win.print(0, 0, "Dommage!");
	this->win.print(0, 1, "Vous avez raté votre année.");
	this->afficheTexte(0, 3, this->config->getText("texte.continuer.texte"));
}

void TxtJeu::afficheCarte() {
	const Carte& carte = this->partie->getConstCarte();
	const Joueur& joueur = this->partie->getConstJoueur();
	int offX = fmax(0, fmin(carte.getLargeur() - this->maxW,
	                        joueur.getPositionX() - this->maxW / 2.0)),
		offY = fmax(0, fmin(carte.getHauteur() - this->maxH,
	                        joueur.getPositionY() - this->maxH / 2.0));

	// Affichage carte
	for (int y = 0; y < (int)carte.getHauteur(); y++) {
		if (y < offY || y >= offY + this->maxH)
			continue;
		for (int x = 0; x < (int)carte.getLargeur(); x++) {
			if (x < offX || x >= offX + this->maxW)
				continue;
			this->affichePixel((x - offX) * this->zoom, (y - offY) * this->zoom,
			                   this->texTuiles[carte.getTexture(x, y)]);
		}
	}

	// Affichage joueur
	this->affichePixel((joueur.getPositionX() - offX) * this->zoom,
	                   (joueur.getPositionY() - offY) * this->zoom,
	                   this->texJoueur[joueur.texture]);

	// Affichage ennemis
	for (const auto& ennemi : this->partie->getConstEnnemis()) {
		if (ennemi->getPositionX() < offX ||
		    ennemi->getPositionX() >= offX + this->maxW ||
		    ennemi->getPositionY() < offY ||
		    ennemi->getPositionY() >= offY + this->maxH)
			continue;
		this->affichePixel((ennemi->getPositionX() - offX) * this->zoom,
		                   (ennemi->getPositionY() - offY) * this->zoom,
		                   this->texEnnemis[ennemi->texture]);
	}

	// Affichage projectiles
	for (const auto& projectile : this->partie->getConstProjectiles()) {
		if (projectile->getPositionX() < offX ||
		    projectile->getPositionX() >= offX + this->maxW ||
		    projectile->getPositionY() < offY ||
		    projectile->getPositionY() >= offY + this->maxH)
			continue;
		this->affichePixel((projectile->getPositionX() - offX) * this->zoom,
		                   (projectile->getPositionY() - offY) * this->zoom,
		                   this->texTuiles[projectile->texture]);
	}
}

void TxtJeu::afficheInterface() {
	int minY = this->maxH * this->zoom;
	const Joueur& joueur = this->partie->getConstJoueur();
	char txtPvJoueur[this->config->getInt("texte.interface.pvJoueur.longueur")];
	sprintf(
		txtPvJoueur, "%s%4.1f%s%4.1f%s",
		this->config->getString("texte.interface.pvJoueur.texteGauche").c_str(),
		(double)joueur.getPV() / 2.0,
		this->config->getString("texte.interface.pvJoueur.texteMilieu").c_str(),
		(double)joueur.getPVMax() / 2.0,
		this->config->getString("texte.interface.pvJoueur.texteDroite")
			.c_str());
	this->win.print(0, minY, txtPvJoueur);
	this->win.print(0, minY + 1, "Inventaire:");
	for (unsigned int i = 0; i < joueur.inventaire.size(); i++) {
		this->win.print(12 + i * 2, minY + 1,
		                this->texTuiles[joueur.inventaire[i]->texture]);
	}
	for (const auto& dialogue : this->partie->getConstDialogues()) {
		if (dialogue.x == (unsigned int)(joueur.getPositionX() + 0.5) &&
		    dialogue.y == (unsigned int)ceil(joueur.getPositionY())) {
			this->afficheTexte(0, minY + 2, dialogue.texte);
		}
	}
}

void TxtJeu::affichePixel(int x, int y, char texture) {
	for (int dy = 0; dy < this->zoom; dy++) {
		for (int dx = 0; dx < this->zoom; dx++) {
			this->win.print(x + dx, y + dy, texture);
		}
	}
}

void TxtJeu::afficheTexte(int x, int y, const std::vector<std::string>& texte) {
	for (unsigned int i = 0; i < texte.size(); i++) {
		this->win.print(x, y + i, texte[i].c_str());
	}
}
