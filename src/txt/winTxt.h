#ifndef _WINTXT_H
#define _WINTXT_H

#include "../carte/Carte.h"

/**
 * @brief une fenêtre texte est un tableau 2D de caractères
 */
class WinTXT {
private:
	/**
	 * @brief largeur
	 */
	int dimx;

	/**
	 * @brief hauteur
	 */
	int dimy;

	/**
	 * @brief stocke le contenu de la fenètre dans un tableau 1D mais on y accede en 2D
	 */
	char* win;

public:
	WinTXT(int dx, int dy);
	~WinTXT();
	void clear(char c = ' ');
	void print(int x, int y, char c);
	void print(int x, int y, const char* c);
	void draw();
	void pause();
	char getCh();
};

void termClear();

#endif
