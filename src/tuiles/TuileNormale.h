#ifndef _TUILE_NORMALE_H
#define _TUILE_NORMALE_H

#include "Tuile.h"

/**
 * @brief Une tuile représentant un sol ou un mur.
 */
class TuileNormale : public Tuile {
public:
	/**
	 * @brief Construit une tuile normale.
	 */
	TuileNormale(int collision);

	/**
	 * @brief Détruit la tuile normale.
	 */
	~TuileNormale();

	/**
	 * @brief Ne fait rien.
	 * @param entite L'entité.
	 * @param x La position x de la tuile.
	 * @param y La position y de la tuile.
	 * @param changements Le `vector` dans lequel les changements de tuiles effectués seront écrits.
	 */
	void subirTuile(Entite& entite, unsigned int x, unsigned int y,
	                std::vector<ChangementTuile>& changements);
};

#endif
