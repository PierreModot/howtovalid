#ifndef _TUILE_H
#define _TUILE_H

#include "../entites/Entite.h"
#include "ChangementTuile.h"
#include <vector>

/**
 * @brief Une tuile.
 */
class Tuile {
public:
	/**
	 * @brief Le niveau de collision de la tuile.
	 */
	int collision;

	/**
	 * @brief Détruit la tuile.
	 */
	virtual ~Tuile() = 0;

	/**
	 * @brief Applique l'effet de la tuile à une entité.
	 * @param entite L'entité.
	 * @param x La position x de la tuile.
	 * @param y La position y de la tuile.
	 * @param changements Le `vector` dans lequel les changements de tuiles effectués seront écrits.
	 */
	virtual void subirTuile(Entite& entite, unsigned int x, unsigned int y,
	                        std::vector<ChangementTuile>& changements) = 0;

protected:
	/**
	 * @brief Construit une tuile.
	 * @param collision Son niveau de collision.
	 */
	Tuile(int collision);
};

#endif
