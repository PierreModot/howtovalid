#ifndef _TUILE_OBJECTIF_H
#define _TUILE_OBJECTIF_H

#include "Tuile.h"

/**
 * @brief Une tuile représentant l'objectif à atteindre.
 */
class TuileObjectif : public Tuile {
public:
	/**
	 * @brief Construit une tuile objectif.
	 */
	TuileObjectif();

	/**
	 * @brief Détruit la tuile objectif.
	 */
	~TuileObjectif();

	/**
	 * @brief Indique à une entité qu'elle a atteint un objectif.
	 * @param entite L'entité.
	 * @param x La position x de la tuile.
	 * @param y La position y de la tuile.
	 * @param changements Le `vector` dans lequel les changements de tuiles effectués seront écrits.
	 */
	void subirTuile(Entite& entite, unsigned int x, unsigned int y,
	                std::vector<ChangementTuile>& changements);
};

#endif
