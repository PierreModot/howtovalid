#ifndef _TUILE_OBJET_H
#define _TUILE_OBJET_H

#include "../objets/Objet.h"
#include "Tuile.h"

/**
 * @brief Une tuile donnant un objet.
 */
class TuileObjet : public Tuile {
public:
	/**
	 * @brief Construit une tuile objet.
	 * @param objet L'objet donné par la tuile. L'objet constuit prend possession de l'objet pointé.
	 * @param objets La liste de objets existants.
	 */
	TuileObjet(Objet* objet, std::vector<ChangementTuile> objets);

	/**
	 * @brief Détruit la tuile objet.
	 */
	~TuileObjet();

	/**
	 * @brief Donne un objet à une entité et remplace la tuile.
	 * @param entite L'entité.
	 * @param x La position x de la tuile.
	 * @param y La position y de la tuile.
	 * @param changements Le `vector` dans lequel les changements de tuiles effectués seront écrits.
	 */
	void subirTuile(Entite& entite, unsigned int x, unsigned int y,
	                std::vector<ChangementTuile>& changements);

private:
	/**
	 * @brief L'objet donné.
	 */
	Objet* objet;

	/**
	 * @brief La liste des objets existants.
	 */
	std::vector<ChangementTuile> objets;
};

#endif
