#include "TuileBlessante.h"

TuileBlessante::TuileBlessante(int valeurDegats) : Tuile(0) {
	this->degats = valeurDegats;
}

TuileBlessante::~TuileBlessante() {}

void TuileBlessante::subirTuile(Entite& entite, unsigned int x, unsigned int y,
                                std::vector<ChangementTuile>& changements) {
	entite.subirDegats(this->degats);
}
