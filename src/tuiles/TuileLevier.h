#ifndef _TUILE_LEVIER_H
#define _TUILE_LEVIER_H

#include "Tuile.h"

/**
 * @brief Une tuile modifiant d'autres tuiles.
 */
class TuileLevier : public Tuile {
public:
	/**
	 * @brief Construit une tuile levier.
	 * @param leviers La liste des leviers existants.
	 */
	TuileLevier(std::vector<std::vector<unsigned int>> leviers);

	/**
	 * @brief Détruit la tuile levier.
	 */
	~TuileLevier();

	/**
	 * @brief Modifie certaines tuiles.
	 * @param entite L'entité.
	 * @param x La position x de la tuile.
	 * @param y La position y de la tuile.
	 * @param changements Le `vector` dans lequel les changements de tuiles effectués seront écrits.
	 */
	void subirTuile(Entite& entite, unsigned int x, unsigned int y,
	                std::vector<ChangementTuile>& changements);

private:
	/**
	 * @brief La liste des leviers existants.
	 */
	std::vector<std::vector<unsigned int>> leviers;
};

#endif
