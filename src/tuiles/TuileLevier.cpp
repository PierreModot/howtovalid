#include "TuileLevier.h"

TuileLevier::TuileLevier(std::vector<std::vector<unsigned int>> leviers)
	: Tuile(0) {
	this->leviers = leviers;
}

TuileLevier::~TuileLevier() {}

void TuileLevier::subirTuile(Entite& entite, unsigned int x, unsigned int y,
                             std::vector<ChangementTuile>& changements) {
	for (const auto& levier : this->leviers) {
		// Les deux premières valeurs sont la position du levier (x,y)
		if (levier.at(0) == x && levier.at(1) == y) {
			// Les valeurs suivantes sont les changements de tuiles (x,y,type,texture) par groupes de 4
			for (unsigned int i = 2; i < levier.size(); i += 4) {
				ChangementTuile chg;
				chg.x = levier.at(i);
				chg.y = levier.at(i + 1);
				chg.type = levier.at(i + 2);
				chg.texture = levier.at(i + 3);
				changements.push_back(chg);
			}
			break;
		}
	}
}
