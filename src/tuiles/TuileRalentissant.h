#ifndef _TUILE_RALENTISSANT_H
#define _TUILE_RALENTISSANT_H

#include "Tuile.h"

/**
 * @brief Une tuile affectant la vitesse de déplacement.
 */
class TuileRalentissant : public Tuile {
public:
	/**
	 * @brief Construit une tuile ralentissant.
	 * @param vitesse Le malus de vitesse donné par la tuile.
	 */
	TuileRalentissant(double vitesse);

	/**
	 * @brief Détruit la tuile ralentissant.
	 */
	~TuileRalentissant();

	/**
	 * @brief Relentit une entité.
	 * @param entite L'entité.
	 * @param x La position x de la tuile.
	 * @param y La position y de la tuile.
	 * @param changements Le `vector` dans lequel les changements de tuiles effectués seront écrits.
	 */
	void subirTuile(Entite& entite, unsigned int x, unsigned int y,
	                std::vector<ChangementTuile>& changements);

private:
	/**
	 * @brief Le malus de vitesse donné par la tuile.
	 */
	double malusVitesse;
};

#endif
