#include "TuileObjet.h"

TuileObjet::TuileObjet(Objet* objet, std::vector<ChangementTuile> objets)
	: Tuile(0) {
	this->objets = objets;
	this->objet = objet;
}

TuileObjet::~TuileObjet() { delete objet; }

void TuileObjet::subirTuile(Entite& entite, unsigned int x, unsigned int y,
                            std::vector<ChangementTuile>& changements) {
	std::vector<Objet*>& inventaire = entite.inventaire;
	// On n'active les effets de la tuile que si l'entité a de la place dans son inventaire
	if (inventaire.size() < entite.tailleMaxInventaire) {
		inventaire.push_back(this->objet);
		for (const auto& objet : this->objets) {
			// On replace la tuile par le changement correspondant
			if (objet.x == x && objet.y == y) {
				changements.push_back(objet);
				break;
			}
		}
	}
}
