#ifndef _OBJET_H
#define _OBJET_H

#include "../entites/Entite.h"

/**
 * @brief Un objet.
 */
class Objet {
public:
	/**
	 * @brief L'indice de la texture utilisée par l'objet.
	 */
	int texture;

	/**
	 * @brief Construit un objet.
	 */
	Objet();

	/**
	 * @brief Détruit l'objet.
	 */
	virtual ~Objet() = 0;

	/**
	 * @brief Utilise l'objet sur une entité.
	 */
	virtual bool utiliser(Entite& entite) const;
};

#endif
