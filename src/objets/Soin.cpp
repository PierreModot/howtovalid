#include "Soin.h"

Soin::Soin(unsigned int pvBonus) {
	this->pvBonus = pvBonus;
	this->texture = 84;
}

Soin::~Soin() {}

bool Soin::utiliser(Entite& entite) const {
	if (entite.getPV() != entite.getPVMax()) {
		entite.soigner(this->pvBonus);
		return true;
	}
	return false;
}
