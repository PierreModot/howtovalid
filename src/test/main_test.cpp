#include "../carte/Carte.h"
#include "../entites/Ennemi.h"
#include "../entites/Joueur.h"
#include "../objets/Soin.h"
#include "../tuiles/ChangementTuile.h"
#include "../tuiles/Tuile.h"
#include "../tuiles/TuileBlessante.h"
#include "../tuiles/TuileLevier.h"
#include "../tuiles/TuileNormale.h"
#include "../tuiles/TuileObjectif.h"
#include "../tuiles/TuileObjet.h"
#include "../tuiles/TuileRalentissant.h"
#include <assert.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include <vector>

void test_carte() {
	// Création fichier carte
	std::ofstream fichierCarteO("tmp/fichierCarte");
	if (!fichierCarteO.is_open()) {
		std::cout
			<< "ERREUR: Impossible d'ouvrir le ficher de carte en écriture"
			<< std::endl;
		return;
	}

	// Leviers
	fichierCarteO << "0" << std::endl;

	// Objets
	fichierCarteO << "0" << std::endl;

	// Tuiles
	fichierCarteO << "5 5" << std::endl;
	fichierCarteO << ". 0 . 0 . 0 . 0 . 0" << std::endl;
	fichierCarteO << "# 1 # 1 # 1 # 1 # 1" << std::endl;
	fichierCarteO << "! 2 ! 2 ! 2 ! 2 ! 2" << std::endl;
	fichierCarteO << "* 3 * 3 * 3 * 3 * 3" << std::endl;
	fichierCarteO << "~ 4 ~ 4 ~ 4 ~ 4 ~ 4" << std::endl;

	// Fermeture du fichier
	fichierCarteO.close();

	// Lecture carte
	std::ifstream fichierCarteI("tmp/fichierCarte");
	if (!fichierCarteI.is_open()) {
		std::cout
			<< "ERREUR: Impossible d'ouvrir le fichier de carte en lecture"
			<< std::endl;
		return;
	}
	Carte carte(fichierCarteI);
	fichierCarteI.close();

	// Test taille
	assert(carte.getLargeur() == 5);
	assert(carte.getHauteur() == 5);

	// Test conditions initiales
	for (int i = 0; i < 5; i++) {
		assert(carte.getType(i, 0) == '.');
		assert(carte.getType(i, 1) == '#');
		assert(carte.getType(i, 2) == '!');
		assert(carte.getType(i, 3) == '*');
		assert(carte.getType(i, 4) == '~');
		assert(carte.getTexture(i, 0) == 0);
		assert(carte.getTexture(i, 1) == 1);
		assert(carte.getTexture(i, 2) == 2);
		assert(carte.getTexture(i, 3) == 3);
		assert(carte.getTexture(i, 4) == 4);
	}

	// Test modifications
	carte.setTuile(1, 1, carte.getTuile(0, 0));
	assert(carte.getTuile(1, 1) == carte.getTuile(0, 0));
	carte.setType(1, 2, '.');
	assert(carte.getType(1, 2) == carte.getType(0, 0));
	carte.setTexture(1, 1, 0);
	assert(carte.getTexture(1, 1) == 0);
}

void test_entites() {
	// JOUEUR
	Joueur j1(1, 1);
	// Tests deplacement joueur
	assert(j1.getPositionX() == 1 && j1.getPositionY() == 1);

	j1.placer(20, 28);
	assert(j1.getPositionX() == 20 && j1.getPositionY() == 28);

	j1.deplacerBas(0.5);
	assert(j1.getPositionX() == 20 && j1.getPositionY() == 28.5);
	j1.deplacerHaut(0.5);
	assert(j1.getPositionX() == 20 && j1.getPositionY() == 28);
	j1.deplacerDroite(0.7);
	assert(j1.getPositionX() == 20.7 && j1.getPositionY() == 28);
	j1.deplacerGauche(0.7);
	assert(j1.getPositionX() == 20 && j1.getPositionY() == 28);

	j1.deplacerBas(5);
	assert(j1.getPositionX() == 20 && j1.getPositionY() == 29);
	j1.deplacerHaut(9);
	assert(j1.getPositionX() == 20 && j1.getPositionY() == 28);
	j1.deplacerDroite(50);
	assert(j1.getPositionX() == 21 && j1.getPositionY() == 28);
	j1.deplacerGauche(500);
	assert(j1.getPositionX() == 20 && j1.getPositionY() == 28);

	// Tests stats joueur
	assert(j1.getDegats() == 4);
	assert(j1.getPV() == j1.getPVMax());

	j1.subirDegats(5);
	assert(j1.getPV() == j1.getPVMax() - 5);

	j1.soigner(5);
	assert(j1.getPV() == j1.getPVMax());

	j1.attaquer(&j1);
	assert(j1.getPV() == (j1.getPVMax() - j1.getDegats()));

	j1.soigner(5000);
	assert(j1.getPV() == j1.getPVMax());

	assert(j1.getVitesse() == 1);
	j1.changerVitesse(5);
	assert(j1.getVitesse() == 5);

	j1.resetVitesse();
	assert(j1.getVitesse() == j1.getVitesseDefaut());

	j1.subirDegats(5000);
	assert(j1.getPV() == 0);

	std::cout << "Test sur le joueur passé avec succès" << std::endl;

	// ENNEMI

	// Création fichier ennemi
	std::ofstream fichierEnnemisO("tmp/fichierEnnemis");
	if (!fichierEnnemisO.is_open()) {
		std::cout
			<< "ERREUR: Impossible d'ouvrir le ficher d'ennemis en écriture"
			<< std::endl;
		return;
	}

	// Placement des ennemis
	fichierEnnemisO << "20 20 5" << std::endl;

	// Fermeture du fichier
	fichierEnnemisO.close();

	// Lecture ennemis
	std::ifstream fichierEnnemisI("tmp/fichierEnnemis");
	if (!fichierEnnemisI.is_open()) {
		std::cout
			<< "ERREUR: Impossible d'ouvrir le fichier d'ennemis en lecture"
			<< std::endl;
		return;
	}
	Ennemi e1(fichierEnnemisI);
	fichierEnnemisI.close();

	assert(e1.getPV() == e1.getPVMax());

	assert(e1.getPositionX() == 20 && e1.getPositionY() == 20);

	e1.placer(201, 28);
	assert(e1.getPositionX() == 201 && e1.getPositionY() == 28);

	e1.deplacerBas(0.5);
	assert(e1.getPositionX() == 201 && e1.getPositionY() == 28.5);
	e1.deplacerHaut(0.5);
	assert(e1.getPositionX() == 201 && e1.getPositionY() == 28);
	e1.deplacerDroite(0.7);
	assert(e1.getPositionX() == 201.7 && e1.getPositionY() == 28);
	e1.deplacerGauche(0.7);
	assert(e1.getPositionX() == 201 && e1.getPositionY() == 28);

	e1.deplacerBas(5);
	assert(e1.getPositionX() == 201 && e1.getPositionY() == 29);
	e1.deplacerHaut(9);
	assert(e1.getPositionX() == 201 && e1.getPositionY() == 28);
	e1.deplacerDroite(50);
	assert(e1.getPositionX() == 202 && e1.getPositionY() == 28);
	e1.deplacerGauche(500);
	assert(e1.getPositionX() == 201 && e1.getPositionY() == 28);

	// Tests stats ennemi
	assert(e1.getDegats() == 2);
	assert(e1.getPV() == e1.getPVMax());

	e1.subirDegats(1);
	assert(e1.getPV() == (e1.getPVMax() - 1));

	e1.soigner(1);
	assert(e1.getPV() == e1.getPVMax());

	e1.attaquer(&e1);
	assert(e1.getPV() == (e1.getPVMax() - e1.getDegats()));

	e1.soigner(5000);
	assert(e1.getPV() == e1.getPVMax());

	assert(e1.getVitesse() == j1.getVitesseDefaut());
	e1.changerVitesse(5);
	assert(e1.getVitesse() == 5);

	e1.resetVitesse();
	assert(e1.getVitesse() == e1.getVitesseDefaut());

	e1.subirDegats(5000);
	assert(e1.getPV() == 0);

	std::cout << "Test sur les ennemies passé avec succès" << std::endl;
}

void test_tuiles() {
	std::cout << "Initialisation" << std::endl;

	int DEGATS = 2;
	double RALENTISSEMENT = 0.5;
	unsigned int SOIN = 40;
	int COLLISION = 5;
	std::vector<std::vector<unsigned int>> LEVIERS;
	std::vector<unsigned int> levier;
	levier.push_back(1);
	levier.push_back(1);
	LEVIERS.push_back(levier);
	levier = std::vector<unsigned int>();
	levier.push_back(1);
	levier.push_back(2);
	levier.push_back(2);
	levier.push_back(1);
	levier.push_back('.');
	levier.push_back(11);
	LEVIERS.push_back(levier);
	levier = std::vector<unsigned int>();
	levier.push_back(1);
	levier.push_back(3);
	levier.push_back(3);
	levier.push_back(1);
	levier.push_back(';');
	levier.push_back(21);
	levier.push_back(3);
	levier.push_back(2);
	levier.push_back('#');
	levier.push_back(22);
	LEVIERS.push_back(levier);
	std::vector<ChangementTuile> OBJETS;
	OBJETS.push_back({1, 2, '.', 11});
	OBJETS.push_back({2, 3, ';', 21});
	Objet* OBJET = new Soin(5);

	Joueur* joueur = new Joueur(0.0, 0.0);
	Tuile* tuile;
	std::vector<ChangementTuile> changements;
	unsigned int pvAvant;
	double vitesseAvant;

	std::cout << "-> TuileBlessante" << std::endl;
	tuile = new TuileBlessante(DEGATS);
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, 0, 0, changements);
	assert(changements.size() == 0);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == fmax(pvAvant - DEGATS, 0.0));
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 0);
	delete tuile;

	std::cout << "-> TuileLevier" << std::endl;
	tuile = new TuileLevier(LEVIERS);
	std::cout << "   -> vide" << std::endl;
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, 0, 0, changements);
	assert(changements.size() == 0);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 0);
	std::cout << "   -> 0 changements" << std::endl;
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, LEVIERS[0][0], LEVIERS[0][1], changements);
	assert(changements.size() == 0);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 0);
	std::cout << "   -> 1 changement" << std::endl;
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, LEVIERS[1][0], LEVIERS[1][1], changements);
	assert(changements.size() == 1);
	assert(changements[0].x == LEVIERS[1][2]);
	assert(changements[0].y == LEVIERS[1][3]);
	assert(changements[0].type == (char)LEVIERS[1][4]);
	assert(changements[0].texture == LEVIERS[1][5]);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 0);
	std::cout << "   -> 2 changements" << std::endl;
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, LEVIERS[2][0], LEVIERS[2][1], changements);
	assert(changements.size() == 2);
	assert(changements[0].x == LEVIERS[2][2]);
	assert(changements[0].y == LEVIERS[2][3]);
	assert(changements[0].type == (char)LEVIERS[2][4]);
	assert(changements[0].texture == LEVIERS[2][5]);
	assert(changements[1].x == LEVIERS[2][6]);
	assert(changements[1].y == LEVIERS[2][7]);
	assert(changements[1].type == (char)LEVIERS[2][8]);
	assert(changements[1].texture == LEVIERS[2][9]);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 0);
	delete tuile;

	std::cout << "-> TuileNormale" << std::endl;
	tuile = new TuileNormale(COLLISION);
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, 0, 0, changements);
	assert(changements.size() == 0);
	assert(tuile->collision == COLLISION);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 0);
	delete tuile;

	std::cout << "-> TuileObjectif" << std::endl;
	tuile = new TuileObjectif();
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, 0, 0, changements);
	assert(changements.size() == 0);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == true);
	assert(joueur->inventaire.size() == 0);
	delete tuile;

	std::cout << "-> TuileObjet" << std::endl;
	tuile = new TuileObjet(OBJET, OBJETS);
	std::cout << "   -> vide" << std::endl;
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, 0, 0, changements);
	assert(changements.size() == 0);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 1);
	assert(joueur->inventaire[0] == OBJET);
	std::cout << "   -> objet 0" << std::endl;
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, OBJETS[0].x, OBJETS[0].y, changements);
	assert(changements.size() == 1);
	assert(changements[0].x == OBJETS[0].x);
	assert(changements[0].y == OBJETS[0].y);
	assert(changements[0].type == OBJETS[0].type);
	assert(changements[0].texture == OBJETS[0].texture);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 1);
	assert(joueur->inventaire[0] == OBJET);
	std::cout << "   -> objet 1" << std::endl;
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, OBJETS[1].x, OBJETS[1].y, changements);
	assert(changements.size() == 1);
	assert(changements[0].x == OBJETS[1].x);
	assert(changements[0].y == OBJETS[1].y);
	assert(changements[0].type == OBJETS[1].type);
	assert(changements[0].texture == OBJETS[1].texture);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == vitesseAvant);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 1);
	assert(joueur->inventaire[0] == OBJET);
	delete tuile;

	std::cout << "-> TuileRalentissant" << std::endl;
	tuile = new TuileRalentissant(RALENTISSEMENT);
	joueur->setPV(SOIN);
	joueur->resetVitesse();
	joueur->gagne = false;
	joueur->inventaire.clear();
	pvAvant = joueur->getPV();
	vitesseAvant = joueur->getVitesse();
	changements.clear();
	tuile->subirTuile(*joueur, 0, 0, changements);
	assert(changements.size() == 0);
	assert(tuile->collision == 0);
	assert(joueur->getPV() == pvAvant);
	assert(joueur->getVitesse() == RALENTISSEMENT);
	assert(joueur->gagne == false);
	assert(joueur->inventaire.size() == 0);
	delete tuile;

	std::cout << "Ok" << std::endl;
	delete joueur;
}

int main(int argc, char** argv) {
	std::cout << "==== TESTS CARTE ... ====" << std::endl;
	test_carte();
	std::cout << "==== TESTS CARTE OK! ====" << std::endl;
	std::cout << "==== TESTS ENTITES ... ====" << std::endl;
	test_entites();
	std::cout << "==== TESTS ENTITES OK! ====" << std::endl;
	std::cout << "==== TESTS TUILES ... ====" << std::endl;
	test_tuiles();
	std::cout << "==== TESTS TUILES OK! ====" << std::endl;
	return 0;
}
