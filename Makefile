CC = g++
CCFLAGS = -Wall -std=c++20 -ggdb
INCLUDES = -I/usr/include/SDL2

LD = g++
LDFLAGS =
LIBS =

LIBS_TXT =
LIBS_SDL = -lSDL2 -lSDL2_image -lSDL2_ttf
LIBS_TEST =

all: make_dirs bin/HowToValid bin/HowToValid_console bin/HowToValid_tests
nothing:
docs:
	doxygen doc/Doxyfile
format:
	clang-format -i src/**/*
clean:
	rm -rf obj/* bin/* doc/html tmp/*
make_dirs:
	mkdir -p src obj bin data doc tmp obj/carte obj/entites obj/objets obj/partie obj/sdl obj/test obj/tuiles obj/txt
bin/HowToValid: obj/carte/Carte.o obj/entites/Ennemi.o obj/entites/EnnemiLanceur.o obj/entites/EnnemiMalin.o obj/entites/Entite.o obj/entites/Joueur.o obj/entites/Projectile.o obj/objets/Objet.o obj/objets/Soin.o obj/partie/Config.o obj/partie/Particule.o obj/partie/Partie.o obj/sdl/main_sdl.o obj/sdl/sdlWin.o obj/tuiles/Tuile.o obj/tuiles/TuileBlessante.o obj/tuiles/TuileLevier.o obj/tuiles/TuileNormale.o obj/tuiles/TuileObjectif.o obj/tuiles/TuileObjet.o obj/tuiles/TuileRalentissant.o
	$(LD) obj/carte/Carte.o obj/entites/Ennemi.o obj/entites/EnnemiLanceur.o obj/entites/EnnemiMalin.o obj/entites/Entite.o obj/entites/Joueur.o obj/entites/Projectile.o obj/objets/Objet.o obj/objets/Soin.o obj/partie/Config.o obj/partie/Particule.o obj/partie/Partie.o obj/sdl/main_sdl.o obj/sdl/sdlWin.o obj/tuiles/Tuile.o obj/tuiles/TuileBlessante.o obj/tuiles/TuileLevier.o obj/tuiles/TuileNormale.o obj/tuiles/TuileObjectif.o obj/tuiles/TuileObjet.o obj/tuiles/TuileRalentissant.o -o bin/HowToValid $(LDFLAGS) $(LIBS) $(LIBS_SDL)
bin/HowToValid_console: obj/carte/Carte.o obj/entites/Ennemi.o obj/entites/EnnemiLanceur.o obj/entites/EnnemiMalin.o obj/entites/Entite.o obj/entites/Joueur.o obj/entites/Projectile.o obj/objets/Objet.o obj/objets/Soin.o obj/partie/Config.o obj/partie/Particule.o obj/partie/Partie.o obj/tuiles/Tuile.o obj/tuiles/TuileBlessante.o obj/tuiles/TuileLevier.o obj/tuiles/TuileNormale.o obj/tuiles/TuileObjectif.o obj/tuiles/TuileObjet.o obj/tuiles/TuileRalentissant.o obj/txt/main_txt.o obj/txt/txtJeu.o obj/txt/winTxt.o
	$(LD) obj/carte/Carte.o obj/entites/Ennemi.o obj/entites/EnnemiLanceur.o obj/entites/EnnemiMalin.o obj/entites/Entite.o obj/entites/Joueur.o obj/entites/Projectile.o obj/objets/Objet.o obj/objets/Soin.o obj/partie/Config.o obj/partie/Particule.o obj/partie/Partie.o obj/tuiles/Tuile.o obj/tuiles/TuileBlessante.o obj/tuiles/TuileLevier.o obj/tuiles/TuileNormale.o obj/tuiles/TuileObjectif.o obj/tuiles/TuileObjet.o obj/tuiles/TuileRalentissant.o obj/txt/main_txt.o obj/txt/txtJeu.o obj/txt/winTxt.o -o bin/HowToValid_console $(LDFLAGS) $(LIBS) $(LIBS_TXT)
bin/HowToValid_tests: obj/carte/Carte.o obj/entites/Ennemi.o obj/entites/EnnemiLanceur.o obj/entites/EnnemiMalin.o obj/entites/Entite.o obj/entites/Joueur.o obj/entites/Projectile.o obj/objets/Objet.o obj/objets/Soin.o obj/partie/Config.o obj/partie/Particule.o obj/partie/Partie.o obj/test/main_test.o obj/tuiles/Tuile.o obj/tuiles/TuileBlessante.o obj/tuiles/TuileLevier.o obj/tuiles/TuileNormale.o obj/tuiles/TuileObjectif.o obj/tuiles/TuileObjet.o obj/tuiles/TuileRalentissant.o
	$(LD) obj/carte/Carte.o obj/entites/Ennemi.o obj/entites/EnnemiLanceur.o obj/entites/EnnemiMalin.o obj/entites/Entite.o obj/entites/Joueur.o obj/entites/Projectile.o obj/objets/Objet.o obj/objets/Soin.o obj/partie/Config.o obj/partie/Particule.o obj/partie/Partie.o obj/test/main_test.o obj/tuiles/Tuile.o obj/tuiles/TuileBlessante.o obj/tuiles/TuileLevier.o obj/tuiles/TuileNormale.o obj/tuiles/TuileObjectif.o obj/tuiles/TuileObjet.o obj/tuiles/TuileRalentissant.o -o bin/HowToValid_tests $(LDFLAGS) $(LIBS) $(LIBS_TEST)
obj/carte/Carte.o: src/carte/Carte.cpp src/carte/Carte.h src/objets/Soin.h src/tuiles/TuileBlessante.h src/tuiles/TuileLevier.h src/tuiles/TuileNormale.h src/tuiles/TuileObjectif.h src/tuiles/TuileObjet.h src/tuiles/TuileRalentissant.h src/tuiles/Tuile.h src/carte/CoupleTuileTexture.h src/objets/Objet.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/carte/Carte.cpp -o obj/carte/Carte.o
obj/entites/Ennemi.o: src/entites/Ennemi.cpp src/entites/Ennemi.h src/entites/Entite.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/entites/Ennemi.cpp -o obj/entites/Ennemi.o
obj/entites/EnnemiLanceur.o: src/entites/EnnemiLanceur.cpp src/entites/EnnemiLanceur.h src/entites/Ennemi.h src/entites/Entite.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/entites/EnnemiLanceur.cpp -o obj/entites/EnnemiLanceur.o
obj/entites/EnnemiMalin.o: src/entites/EnnemiMalin.cpp src/entites/EnnemiMalin.h src/entites/Ennemi.h src/entites/Entite.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/entites/EnnemiMalin.cpp -o obj/entites/EnnemiMalin.o
obj/entites/Entite.o: src/entites/Entite.cpp src/entites/Entite.h src/objets/Objet.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/entites/Entite.cpp -o obj/entites/Entite.o
obj/entites/Joueur.o: src/entites/Joueur.cpp src/entites/Joueur.h src/entites/Entite.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/entites/Joueur.cpp -o obj/entites/Joueur.o
obj/entites/Projectile.o: src/entites/Projectile.cpp src/entites/Projectile.h src/entites/Entite.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/entites/Projectile.cpp -o obj/entites/Projectile.o
obj/objets/Objet.o: src/objets/Objet.cpp src/objets/Objet.h src/entites/Entite.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/objets/Objet.cpp -o obj/objets/Objet.o
obj/objets/Soin.o: src/objets/Soin.cpp src/objets/Soin.h src/objets/Objet.h src/entites/Entite.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/objets/Soin.cpp -o obj/objets/Soin.o
obj/partie/Config.o: src/partie/Config.cpp src/partie/Config.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/partie/Config.cpp -o obj/partie/Config.o
obj/partie/Particule.o: src/partie/Particule.cpp src/partie/Particule.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/partie/Particule.cpp -o obj/partie/Particule.o
obj/partie/Partie.o: src/partie/Partie.cpp src/partie/Partie.h src/entites/EnnemiLanceur.h src/entites/EnnemiMalin.h src/carte/Carte.h src/entites/Ennemi.h src/entites/Entite.h src/entites/Joueur.h src/entites/Projectile.h src/partie/Config.h src/partie/Dialogue.h src/partie/Particule.h src/tuiles/Tuile.h src/carte/CoupleTuileTexture.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/partie/Partie.cpp -o obj/partie/Partie.o
obj/sdl/main_sdl.o: src/sdl/main_sdl.cpp src/partie/Config.h src/partie/Partie.h src/sdl/sdlWin.h src/carte/Carte.h src/entites/Ennemi.h src/entites/Entite.h src/entites/Joueur.h src/entites/Projectile.h src/partie/Dialogue.h src/partie/Particule.h src/tuiles/Tuile.h src/carte/CoupleTuileTexture.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/sdl/main_sdl.cpp -o obj/sdl/main_sdl.o
obj/sdl/sdlWin.o: src/sdl/sdlWin.cpp src/sdl/sdlWin.h src/objets/Objet.h src/partie/Config.h src/partie/Partie.h src/entites/Entite.h src/carte/Carte.h src/entites/Ennemi.h src/entites/Joueur.h src/entites/Projectile.h src/partie/Dialogue.h src/partie/Particule.h src/tuiles/Tuile.h src/carte/CoupleTuileTexture.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/sdl/sdlWin.cpp -o obj/sdl/sdlWin.o
obj/test/main_test.o: src/test/main_test.cpp src/carte/Carte.h src/entites/Ennemi.h src/entites/Joueur.h src/objets/Soin.h src/tuiles/ChangementTuile.h src/tuiles/Tuile.h src/tuiles/TuileBlessante.h src/tuiles/TuileLevier.h src/tuiles/TuileNormale.h src/tuiles/TuileObjectif.h src/tuiles/TuileObjet.h src/tuiles/TuileRalentissant.h src/carte/CoupleTuileTexture.h src/entites/Entite.h src/objets/Objet.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/test/main_test.cpp -o obj/test/main_test.o
obj/tuiles/Tuile.o: src/tuiles/Tuile.cpp src/tuiles/Tuile.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/tuiles/Tuile.cpp -o obj/tuiles/Tuile.o
obj/tuiles/TuileBlessante.o: src/tuiles/TuileBlessante.cpp src/tuiles/TuileBlessante.h src/tuiles/Tuile.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/tuiles/TuileBlessante.cpp -o obj/tuiles/TuileBlessante.o
obj/tuiles/TuileLevier.o: src/tuiles/TuileLevier.cpp src/tuiles/TuileLevier.h src/tuiles/Tuile.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/tuiles/TuileLevier.cpp -o obj/tuiles/TuileLevier.o
obj/tuiles/TuileNormale.o: src/tuiles/TuileNormale.cpp src/tuiles/TuileNormale.h src/tuiles/Tuile.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/tuiles/TuileNormale.cpp -o obj/tuiles/TuileNormale.o
obj/tuiles/TuileObjectif.o: src/tuiles/TuileObjectif.cpp src/tuiles/TuileObjectif.h src/tuiles/Tuile.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/tuiles/TuileObjectif.cpp -o obj/tuiles/TuileObjectif.o
obj/tuiles/TuileObjet.o: src/tuiles/TuileObjet.cpp src/tuiles/TuileObjet.h src/objets/Objet.h src/tuiles/Tuile.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/tuiles/TuileObjet.cpp -o obj/tuiles/TuileObjet.o
obj/tuiles/TuileRalentissant.o: src/tuiles/TuileRalentissant.cpp src/tuiles/TuileRalentissant.h src/tuiles/Tuile.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/tuiles/TuileRalentissant.cpp -o obj/tuiles/TuileRalentissant.o
obj/txt/main_txt.o: src/txt/main_txt.cpp src/partie/Config.h src/partie/Partie.h src/txt/txtJeu.h src/txt/winTxt.h src/carte/Carte.h src/entites/Ennemi.h src/entites/Entite.h src/entites/Joueur.h src/entites/Projectile.h src/partie/Dialogue.h src/partie/Particule.h src/tuiles/Tuile.h src/carte/CoupleTuileTexture.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/txt/main_txt.cpp -o obj/txt/main_txt.o
obj/txt/txtJeu.o: src/txt/txtJeu.cpp src/txt/txtJeu.h src/objets/Objet.h src/partie/Config.h src/partie/Partie.h src/txt/winTxt.h src/entites/Entite.h src/carte/Carte.h src/entites/Ennemi.h src/entites/Joueur.h src/entites/Projectile.h src/partie/Dialogue.h src/partie/Particule.h src/tuiles/Tuile.h src/carte/CoupleTuileTexture.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/txt/txtJeu.cpp -o obj/txt/txtJeu.o
obj/txt/winTxt.o: src/txt/winTxt.cpp src/txt/winTxt.h src/carte/Carte.h src/tuiles/Tuile.h src/carte/CoupleTuileTexture.h src/entites/Entite.h src/tuiles/ChangementTuile.h
	$(CC) -c $(CCFLAGS) $(INCLUDES) src/txt/winTxt.cpp -o obj/txt/winTxt.o
